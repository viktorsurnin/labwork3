package ru.leti.computer.graphic.panel;

public interface RotateListener {
    void xRotate(double alpha);

    void yRotate(double beta);

    void setDefaultRotation();
}
