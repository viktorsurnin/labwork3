package ru.leti.computer.graphic.panel;

import ru.leti.computer.graphic.model.Point3D;

import java.util.List;

public interface ControlPanelListener {
    void setBasePoints(List<Point3D> basePoints);
    void setBaseLineVisible(boolean visible);
}
