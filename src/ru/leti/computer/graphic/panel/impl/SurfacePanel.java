package ru.leti.computer.graphic.panel.impl;

import Jama.Matrix;
import ru.leti.computer.graphic.model.Surface;
import ru.leti.computer.graphic.model.Point2D;
import ru.leti.computer.graphic.model.Point3D;
import ru.leti.computer.graphic.mouse.listener.DrawPanelMouseListener;
import ru.leti.computer.graphic.panel.ControlPanelListener;
import ru.leti.computer.graphic.panel.RotateListener;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;

public class SurfacePanel extends JPanel implements RotateListener, ControlPanelListener {

    private static final int AXIS_LENGTH = 350;
    private static final int POINT_SIZE = 6;
    private static final int POLYHEDRON_SIZE = 4;
    private static final double DEGREES_TO_RADIANS = Math.PI / 180;
    private static final double ALPHA = 35.264;
    private static final double BETA = 45;

    private double alphaMouseRotate = 0;
    private double betaMouseRotate = 0;

    private Matrix defaultRotationMatrix;
    private Matrix rotationMatrix;

    private List<Point3D> basePoints;
    private List<List<Point3D>> curveLines;

    private boolean isBaseLineVisible = true;
    private boolean isCurvePointMarked = true;

    public SurfacePanel(int width, int height) {
        setPreferredSize(new Dimension(width, height));
        addMouseListener(new DrawPanelMouseListener(this, width, height));
        updateRotationMatrix();
        defaultRotationMatrix = rotationMatrix;
        basePoints = new ArrayList<>();
    }

    @Override
    public void paint(Graphics g) {


        g.setColor(Color.WHITE);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());


        Graphics2D g2d = (Graphics2D) g;
        AffineTransform offsetToCenter = new AffineTransform();
        offsetToCenter.translate(this.getWidth() / 2, this.getHeight() / 2);
        g2d.transform(offsetToCenter);


        Point3D zeroPoint = new Point3D(0, 0, 0);
        Point3D xAxis = new Point3D(AXIS_LENGTH, 0, 0);
        Point3D yAxis = new Point3D(0, -AXIS_LENGTH, 0);
        Point3D zAxis = new Point3D(0, 0, AXIS_LENGTH);
        drawAxes(g, zeroPoint, xAxis, yAxis, zAxis);



        if (basePoints != null && !basePoints.isEmpty()) {
            curveLines = new ArrayList<>(Surface.plotSurface(basePoints));
        }


        g.setColor(Color.GRAY);
        if (curveLines != null) {
            int size = curveLines.size();
            for (int i = 0; i < size; i++) {
                List<Point3D> line = new ArrayList<>();
                for (int j = 0; j < size; j++) {
                    line.add(curveLines.get(j).get(i));
                }
                curveLines.add(line);
            }
            for (List<Point3D> line : curveLines) {
                Point2D prevPoint = null;
                Point2D currentPoint;
                for (Point3D p : line) {
                    p = new Point3D(p.getX(), -p.getY(), p.getZ());
                    currentPoint = orthogonalProjection(convert(p, rotationMatrix));
                    if (isCurvePointMarked) {
                        drawPointWithMark(g, currentPoint);
                    }
                    if (prevPoint != null) {
                        drawLine(g, prevPoint, currentPoint);
                    }
                    prevPoint = currentPoint;
                }
            }
        }

        if (isBaseLineVisible) {
            g.setColor(Color.BLACK);
            if (basePoints != null) {
                List<Point3D> basePointsCopy = new ArrayList<>(basePoints);
                for (int i = 0; i < basePointsCopy.size(); i++) {
                    Point3D currBasePoint = basePointsCopy.get(i);

                    currBasePoint = reverseByY(currBasePoint);
                    Point2D currBasePoint2D = orthogonalProjection(convert(currBasePoint,
                            rotationMatrix));
                    drawPointWithMark(g, currBasePoint2D);
                    drawString(g, new Point2D(currBasePoint2D.getX() + 10, currBasePoint2D.getY() + 10), String.valueOf(i));

                    boolean isNotEndOfRow = true;
                    for (int j = 1; j < POLYHEDRON_SIZE; j++) {
                        if (i == (POLYHEDRON_SIZE * j - 1)) {
                            isNotEndOfRow = false;
                            break;
                        }
                    }
                    if (isNotEndOfRow && (i + 1 < basePointsCopy.size())) {
                        drawLine(g, currBasePoint2D, orthogonalProjection(
                                convert(reverseByY(basePointsCopy.get(i + 1)), rotationMatrix)));
                    }
                    if (i + 4 < basePointsCopy.size()) {
                        drawLine(g, currBasePoint2D, orthogonalProjection(convert(
                                reverseByY(basePointsCopy.get(i + 4)), rotationMatrix)));
                    }
                }
            }
        }
    }

    @Override
    public void xRotate(double alpha) {
        this.alphaMouseRotate = alpha;
        updateXRotationMatrix();
        repaint();
    }

    @Override
    public void yRotate(double beta) {
        this.betaMouseRotate = beta;
        updateYRotationMatrix();
        repaint();
    }

    @Override
    public void setDefaultRotation() {
        this.alphaMouseRotate = 0;
        this.betaMouseRotate = 0;
        setDefaultRotationMatrix();
        repaint();
    }

    private void setDefaultRotationMatrix() {
        rotationMatrix = getBaseRotation();
    }

    private void updateRotationMatrix() {
        rotationMatrix = getBaseRotation();
    }

    private void updateXRotationMatrix() {
        rotationMatrix = getXRotationMatrix(rotationMatrix, alphaMouseRotate);
    }

    private void updateYRotationMatrix() {
        rotationMatrix = getYRotationMatrix(rotationMatrix, betaMouseRotate);
    }

    private void drawPointWithMark(Graphics g, Point2D p) {
        drawLine(g, p, p);
        int x = (int) p.getX();
        int y = (int) p.getY();
        g.drawRect(x - POINT_SIZE / 2, y - POINT_SIZE / 2, POINT_SIZE, POINT_SIZE);
    }

    private void drawLine(Graphics g, Point2D p1, Point2D p2) {
        int x1 = (int) p1.getX();
        int y1 = (int) p1.getY();
        int x2 = (int) p2.getX();
        int y2 = (int) p2.getY();

        g.drawLine(x1, y1, x2, y2);
    }

    private void drawString(Graphics g, Point2D p, String str) {
        int x = (int) p.getX();
        int y = (int) p.getY();
        g.drawString(str, x, y);
    }

    private void drawAxes(Graphics g, Point3D zeroPoint, Point3D xAxis, Point3D yAxis, Point3D zAxis) {
        xAxis = convert(xAxis, defaultRotationMatrix);
        yAxis = convert(yAxis, defaultRotationMatrix);
        zAxis = convert(zAxis, defaultRotationMatrix);

        Point2D zeroPoint2D = orthogonalProjection(zeroPoint);
        Point2D xAxis2D = orthogonalProjection(xAxis);
        Point2D yAxis2D = orthogonalProjection(yAxis);
        Point2D zAxis2D = orthogonalProjection(zAxis);

        g.setColor(Color.RED);
        drawLine(g, zeroPoint2D, xAxis2D);
        drawString(g, xAxis2D, "X");

        g.setColor(Color.GREEN);
        drawLine(g, zeroPoint2D, yAxis2D);
        drawString(g, yAxis2D, "Y");

        g.setColor(Color.BLUE);
        drawLine(g, zeroPoint2D, zAxis2D);
        drawString(g, zAxis2D, "Z");
    }


    @Override
    public void setBasePoints(List<Point3D> basePoints) {
        this.basePoints = basePoints;
        repaint();
    }

    @Override
    public void setBaseLineVisible(boolean visible) {
        this.isBaseLineVisible = visible;
        repaint();
    }


    public static Point3D reverseByY(Point3D currBasePoint) {
        return new Point3D(currBasePoint.getX(), -currBasePoint.getY(), currBasePoint.getZ());
    }

    public static Matrix getBaseRotation() {
        double[][] matrixAlpha = {
                {1, 0, 0},
                {0, Math.cos(ALPHA * DEGREES_TO_RADIANS), Math.sin(ALPHA * DEGREES_TO_RADIANS)},
                {0, -Math.sin(ALPHA * DEGREES_TO_RADIANS), Math.cos(ALPHA * DEGREES_TO_RADIANS)}
        };
        double[][] matrixBeta = {
                {Math.cos(BETA * DEGREES_TO_RADIANS), 0, -Math.sin(BETA * DEGREES_TO_RADIANS)},
                {0, 1, 0},
                {Math.sin(BETA * DEGREES_TO_RADIANS), 0, Math.cos(BETA * DEGREES_TO_RADIANS)}
        };
        return new Matrix(matrixAlpha).times(new Matrix(matrixBeta));
    }

    public static Matrix getXRotationMatrix(Matrix rotationMatrix, double alpha) {
        alpha *= DEGREES_TO_RADIANS;
        double[][] matrixAlpha = {
                {1, 0, 0},
                {0, Math.cos(alpha), Math.sin(alpha)},
                {0, -Math.sin(alpha), Math.cos(alpha)}
        };
        return rotationMatrix.times(new Matrix(matrixAlpha));
    }

    public static Matrix getYRotationMatrix(Matrix rotationMatrix, double beta) {
        beta *= DEGREES_TO_RADIANS;
        double[][] matrixBeta = {
                {Math.cos(beta), 0, -Math.sin(beta)},
                {0, 1, 0},
                {Math.sin(beta), 0, Math.cos(beta)}
        };
        return rotationMatrix.times(new Matrix(matrixBeta));
    }

    public static Point3D convert(Point3D point3D, Matrix rotationMatrix) {
        double[] pointArray = point3D.getArray();
        double[][] pointMatrix = {
                {pointArray[0]},
                {pointArray[1]},
                {pointArray[2]}
        };
        double[][] rotationPoint = rotationMatrix.times(new Matrix(pointMatrix)).getArray();
        return new Point3D(rotationPoint[0][0], rotationPoint[1][0], rotationPoint[2][0]);
    }

    public static Point2D orthogonalProjection(Point3D point3D) {
        return new Point2D(point3D.getX(), point3D.getY());
    }
}
