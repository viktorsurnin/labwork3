package ru.leti.computer.graphic.panel.impl;

import ru.leti.computer.graphic.model.Point3D;
import ru.leti.computer.graphic.panel.ControlPanelListener;
import ru.leti.computer.graphic.panel.RotateListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SurfaceConfigurationPanel extends JPanel {

    private static final String REPAINT_BUTTON_STR = "Обобразить поверхность";
    private static final String BASE_POINT_STR = "Отображать точки многогранника";
    private static final String DEFAULT_ROTATION_STR = "Установить стандартные углы вращения";
    private static final String POLYHEDRON_VERTICES_STR = "Задать координаты";

    private ControlPanelListener controlPanelListener;

    private JCheckBox isBaseLineVisible;

    private List<JTextField> pointXFields;
    private List<JTextField> pointYFields;
    private List<JTextField> pointZFields;

    private PolyhedronVertices polyhedronVerticesDialog = new PolyhedronVertices();

    public SurfaceConfigurationPanel(SurfacePanel controlPanelListener) {

        this.controlPanelListener = controlPanelListener;

        JPanel buttonPanel = new JPanel(new GridLayout(1, 4, 10, 0));


        CheckBoxListener checkBoxListener = new CheckBoxListener();

        isBaseLineVisible = new JCheckBox(BASE_POINT_STR, true);
        isBaseLineVisible.addActionListener(checkBoxListener);


        ActionListener buttonListener = createActionListener(controlPanelListener);

        createMainControlPanel(buttonPanel, buttonListener);

        polyhedronVerticesDialog.setPoints();
        sendBasePoints();
    }

    private void createMainControlPanel(JPanel buttonPanel, ActionListener buttonListener) {
        JButton repaintButton = new JButton(REPAINT_BUTTON_STR);
        repaintButton.addActionListener(buttonListener);
        JButton setDefaultRotationButton = new JButton(DEFAULT_ROTATION_STR);
        setDefaultRotationButton.addActionListener(buttonListener);
        JButton setPolyhedronVertices = new JButton(POLYHEDRON_VERTICES_STR);
        setPolyhedronVertices.addActionListener(buttonListener);

        buttonPanel.add(repaintButton);
        buttonPanel.add(setDefaultRotationButton);
        buttonPanel.add(setPolyhedronVertices);
        buttonPanel.add(isBaseLineVisible);
        add(buttonPanel);
    }

    private ActionListener createActionListener(RotateListener controlPanelListener) {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getActionCommand().equals(SurfaceConfigurationPanel.REPAINT_BUTTON_STR)) {
                    try {
                        if (isPointsSets()) {
                            sendBasePoints();
                        } else {
                            throw new NumberFormatException();
                        }
                    } catch (NumberFormatException exception) {
                        JOptionPane.showMessageDialog(null, "Заданы некорректные значения координат точек.");
                    }
                } else if (e.getActionCommand().equals(SurfaceConfigurationPanel.DEFAULT_ROTATION_STR)) {
                    controlPanelListener.setDefaultRotation();
                } else if (e.getActionCommand().equals(SurfaceConfigurationPanel.POLYHEDRON_VERTICES_STR)) {
                    polyhedronVerticesDialog.open();
                }
            }

            private boolean isPointsSets() {
                return pointXFields != null
                        && pointYFields != null
                        && pointZFields != null
                        && !pointXFields.isEmpty()
                        && !pointYFields.isEmpty()
                        && !pointZFields.isEmpty();
            }
        };
    }

    public void setPointXFields(List<JTextField> pointXFields) {
        this.pointXFields = pointXFields;
    }

    public void setPointYFields(List<JTextField> pointYFields) {
        this.pointYFields = pointYFields;
    }

    public void setPointZFields(List<JTextField> pointZFields) {
        this.pointZFields = pointZFields;
    }

    private void sendBasePoints() {
        List<Point3D> basePoints = new ArrayList<>();
        double x;
        double y;
        double z;
        for (int i = 0; i < pointXFields.size(); i++) {
            x = Double.valueOf(pointXFields.get(i).getText());
            y = Double.valueOf(pointYFields.get(i).getText());
            z = Double.valueOf(pointZFields.get(i).getText());
            basePoints.add(new Point3D(x, y, z));
        }
        controlPanelListener.setBasePoints(basePoints);
    }

    class CheckBoxListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals(SurfaceConfigurationPanel.BASE_POINT_STR)) {
                controlPanelListener.setBaseLineVisible(isBaseLineVisible.isSelected());
            }
        }
    }

    class PolyhedronVertices extends JDialog {
        private static final String TITLE = "Меню ввода координат многогранника";

        private static final String SET_BASE_POINTS_STR = "Задать вершины многогранника";
        private static final String SET_DEFAULT_BASE_POINTS_STR = "Вершины по умолчанию";

        private PolyhedronVerticesButtonListener buttonListener;


        private double[] xValues = {-150, -150, -150, -150, -50, -50, -50, -50, 50, 50, 50, 50, 150, 150, 150, 150};
        private double[] yValues = {0, 50, 50, 0, 50, -300, 50, 50, 50, 50, 300, 50, 0, 50, 50, 0};
        private double[] zValues = {150, 50, -50, -150, 150, 50, -50, -150, 150, 50, -50, -150, 150, 50, -50, -150};

        private List<JTextField> pointXFields;
        private List<JTextField> pointYFields;
        private List<JTextField> pointZFields;

        PolyhedronVertices() {
            setTitle(TITLE);
            setModal(true);
            setSize(new Dimension(800, 200));
            setResizable(false);
            setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

            buttonListener = new PolyhedronVerticesButtonListener();

            JButton setBasePointsButton = new JButton(SET_BASE_POINTS_STR);
            setBasePointsButton.addActionListener(buttonListener);
            JButton setDefaultPointsButton = new JButton(SET_DEFAULT_BASE_POINTS_STR);
            setDefaultPointsButton.addActionListener(buttonListener);

            JPanel buttonPanel = new JPanel(new GridLayout(1, 3));
            buttonPanel.add(setBasePointsButton);
            buttonPanel.add(setDefaultPointsButton);


            pointXFields = new ArrayList<>();
            pointYFields = new ArrayList<>();
            pointZFields = new ArrayList<>();

            for (int i = 0; i < 16; i++) {
                pointXFields.add(new JTextField(String.valueOf(xValues[i]), 6));
                pointYFields.add(new JTextField(String.valueOf(yValues[i]), 6));
                pointZFields.add(new JTextField(String.valueOf(zValues[i]), 6));
            }

            JPanel coordinatesPanel = new JPanel(new GridLayout(4, 4, 10, 20));
            JPanel currPanel;
            for (int i = 0; i < 16; i++) {
                currPanel = new JPanel(new GridLayout(1, 3));
                currPanel.add(new JLabel(String.valueOf(i)));
                currPanel.add(pointXFields.get(i));
                currPanel.add(pointYFields.get(i));
                currPanel.add(pointZFields.get(i));
                coordinatesPanel.add(currPanel);
            }

            setLayout(new BorderLayout());
            add(coordinatesPanel, BorderLayout.CENTER);
            add(buttonPanel, BorderLayout.SOUTH);
            pack();
            setVisible(false);
        }

        public void open() {
            setVisible(true);
        }


        public void setPoints() {
            setPointXFields(pointXFields);
            setPointYFields(pointYFields);
            setPointZFields(pointZFields);
        }

        public class PolyhedronVerticesButtonListener implements ActionListener, Serializable {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getActionCommand().equals(SET_BASE_POINTS_STR)) {
                    setPoints();
                    close();
                } else if (e.getActionCommand().equals(SET_DEFAULT_BASE_POINTS_STR)) {
                    for (int i = 0; i < 16; i++) {
                        pointXFields.get(i).setText((String.valueOf(xValues[i])));
                        pointYFields.get(i).setText((String.valueOf(yValues[i])));
                        pointZFields.get(i).setText((String.valueOf(zValues[i])));
                    }
                }
            }

            private void close() {
                setVisible(false);
            }

            private double getRandomNumber(double max) {
                int sign = (Math.random() > 0.5) ? -1 : 1;
                return sign * (Math.random() * max);
            }
        }
    }
}

