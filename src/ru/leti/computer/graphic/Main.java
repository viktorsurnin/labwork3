package ru.leti.computer.graphic;

import ru.leti.computer.graphic.window.MainWindow;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        new MainWindow();
    }
}
