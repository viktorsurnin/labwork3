package ru.leti.computer.graphic.mouse.listener;

import ru.leti.computer.graphic.panel.RotateListener;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class DrawPanelMouseListener extends MouseAdapter {

    private RotateListener rotateListener;

    private int maxX;
    private int maxY;

    private int startX;
    private int startY;

    private int endX;
    private int endY;

    public DrawPanelMouseListener(RotateListener rotateListener, int maxX, int maxY) {
        this.rotateListener = rotateListener;
        this.maxX = maxX;
        this.maxY = maxY;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        startX = e.getX();
        startY = e.getY();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        endX = e.getX();
        endY = e.getY();
        rotate();
    }

    private void rotate() {

        double alpha;
        double beta;

        double deltaX = endX - startX;
        double deltaY = endY - startY;

        if (Math.abs(deltaX) > Math.abs(deltaY)) {
            beta = (deltaX / maxX) * 90;
            rotateListener.yRotate(beta);
        } else {
            alpha = (deltaY / maxY) * 90;
            rotateListener.xRotate(alpha);
        }
    }
}