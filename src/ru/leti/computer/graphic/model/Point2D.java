package ru.leti.computer.graphic.model;

public class Point2D {

    private double x;
    private double y;

    public Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double[] getArray() {
        double[] array = {this.getX(), this.getY()};
        return array;
    }


}
