package ru.leti.computer.graphic.model;

import Jama.Matrix;

import java.util.ArrayList;
import java.util.List;

public class Surface {

    private Surface() {
    }

    private static final double T_STEP = 0.05; // шаг переменной t (t принадлежит [0, 1])
    private static final double W_STEP = 0.05; // шаг переменной w (w принадлежит [0, 1])

    private static final double[][] basisMatrix = {
            {-1, 3, -3, 1},
            {3, -6, 3, 0},
            {-3, 3, 0, 0},
            {1, 0, 0, 0}
    };

    public static List<Point3D> getCurvePoints(List<Point3D> basePoints) {
        List<Point3D> curvePoints = new ArrayList<>();
        int i = 0;
        do {
            curvePoints.addAll(getCurvePoints(basePoints.get(i), basePoints.get(i + 1), basePoints.get(i + 2),
                    basePoints.get(i + 3)));
            i += 3;
        } while (i < (basePoints.size() - 1));
        return curvePoints;
    }

    public static List<Point3D> getCurvePoints(Point3D p0, Point3D p1, Point3D p2, Point3D p3) {
        List<Point3D> curvePoints = new ArrayList<>();
        double[][] matrixT = new double[1][4];

        double[][] matrixP = {
                {p0.getX(), p0.getY(), p0.getZ()},
                {p1.getX(), p1.getY(), p1.getZ()},
                {p2.getX(), p2.getY(), p2.getZ()},
                {p3.getX(), p3.getY(), p3.getZ()}
        };
        for (double t = 0; t <= 1.0; t += T_STEP) {

            for (int i = 0; i <= 3; i++) {
                matrixT[0][i] = Math.pow(t, 3 - i);
            }
            double[][] pointMatrixP = (new Matrix(matrixT).times(new Matrix(basisMatrix))).
                    times(new Matrix(matrixP)).getArray();
            curvePoints.add(new Point3D(pointMatrixP[0][0], pointMatrixP[0][1], pointMatrixP[0][2]));
        }
        return curvePoints;
    }

    public static List<List<Point3D>> plotSurface(final List<Point3D> basePoints) {



        double[][] matrixX = new double[4][4];
        double[][] matrixY = new double[4][4];
        double[][] matrixZ = new double[4][4];

        for (int r = 0; r < 4; r++) {
            for (int c = 0; c < 4; c++) {
                matrixX[r][c] = basePoints.get(r * 4 + c).getX();
                matrixY[r][c] = basePoints.get(r * 4 + c).getY();
                matrixZ[r][c] = basePoints.get(r * 4 + c).getZ();
            }
        }

        Matrix basis = new Matrix(basisMatrix);
        Matrix basePointsXMatrixChanged = (basis.times(new Matrix(matrixX))).times(basis);
        Matrix basePointsYMatrixChanged = (basis.times(new Matrix(matrixY))).times(basis);
        Matrix basePointsZMatrixChanged = (basis.times(new Matrix(matrixZ))).times(basis);

        double[][] rawMatrixT = new double[1][4];
        double[][] rawMatrixW = new double[4][1];

        List<List<Point3D>> linesOfSurface = new ArrayList<>();

        for (double t = 0; t <= 1.0; t += T_STEP) {

            for (int i = 0; i <= 3; i++) {
                rawMatrixT[0][i] = Math.pow(t, 3 - i);
            }
            List<Point3D> currLine = new ArrayList<>();
            Matrix matrixT = new Matrix(rawMatrixT);
            for (double w = 0; w <= 1.0; w += W_STEP) {

                for (int i = 0; i <= 3; i++) {
                    rawMatrixW[i][0] = Math.pow(w, 3 - i);
                }
                Matrix matrixW = new Matrix(rawMatrixW);

                double x = (matrixT.times(basePointsXMatrixChanged)).times(matrixW).get(0, 0);
                double y = (matrixT.times(basePointsYMatrixChanged)).times(matrixW).get(0, 0);
                double z = (matrixT.times(basePointsZMatrixChanged)).times(matrixW).get(0, 0);

                currLine.add(new Point3D(x, y, z));
            }
            linesOfSurface.add(currLine);
        }
        return linesOfSurface;
    }
}