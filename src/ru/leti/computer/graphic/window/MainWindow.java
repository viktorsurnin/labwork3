package ru.leti.computer.graphic.window;

import ru.leti.computer.graphic.panel.impl.SurfacePanel;
import ru.leti.computer.graphic.panel.impl.SurfaceConfigurationPanel;

import javax.swing.*;
import java.awt.*;

public class MainWindow extends JFrame {
    private static final int WIDTH = 1050;
    private static final int HEIGHT = 700;
    private static final String TITLE = "Лабораторная работа №3 по дисциплине Компьютерная графика";

    public MainWindow() throws HeadlessException {
        super(TITLE);

        setSize(new Dimension(WIDTH, HEIGHT));

        SurfacePanel surfacePanel = new SurfacePanel(WIDTH, HEIGHT);
        SurfaceConfigurationPanel surfaceConfigurationPanel = new SurfaceConfigurationPanel(surfacePanel);

        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        setLayout(new BorderLayout());
        add(surfacePanel, BorderLayout.CENTER);
        add(surfaceConfigurationPanel, BorderLayout.NORTH);
        pack();
        setVisible(true);
    }
}
